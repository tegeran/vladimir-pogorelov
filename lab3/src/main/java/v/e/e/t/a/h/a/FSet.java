package v.e.e.t.a.h.a;

public interface FSet<T> {
    boolean contains(T suspect);
}

