package v.e.e.t.a.h.a.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import v.e.e.t.a.h.a.*;

import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("List tree")
public class ListTreeTest {
    @Test
    @DisplayName("Empty")
    void ListTreeEmptyTest() {
        ListTree<Integer, String> atd = new ListTree<>();
        assertNotNull(atd);
        assertEquals(0, atd.size());
        assertTrue(atd.isEmpty());
    }

    @Test
    @DisplayName("Put one element")
    void ListTreePutOneElementTest() {
        ListTree<Integer, String> listTree = new ListTree<>();
        assertFalse(listTree.contains(25));
        listTree.put(25, "TeSt");
        assertTrue(listTree.contains(25));
        assertEquals("TeSt", listTree.get(25));
        assertEquals(1, listTree.size());
        assertFalse(listTree.isEmpty());
    }

    @Test
    @DisplayName("Put the equal elements")
    void ListTreePutEqualElementsTest() {
        var listTree = new ListTree<Integer, String>();
        listTree.put(25, "Test");
        listTree.put(25, "Another data");
        assertEquals(1, listTree.size());
        assertEquals("Test", listTree.get(25));
    }

    @Test
    @DisplayName("Remove nonexistent element")
    void ListTreeRemoveNonexistentElementTest() {
        ListTree<Integer, String> listTree = new ListTree<>();
        assertEquals(listTree.remove(12), Optional.empty());
    }

    @Test
    @DisplayName("Remove elements")
    void ListTreeRemoveElementsTest() {
        ListTree<Integer, String> listTree = new ListTree<>();
        listTree.put(1001, "Something");
        listTree.put(40, "test");
        listTree.put(0, "value");
        listTree.put(-5, "");
        assertEquals(4, listTree.size());
        assertEquals("", listTree.remove(-5));
        assertFalse(listTree.contains(-5));
        assertEquals("test", listTree.remove(40));
        assertFalse(listTree.contains(40));
        assertEquals(2, listTree.size());
        assertEquals("Something", listTree.remove(1001));
        assertEquals("value", listTree.remove(0));
        assertTrue(listTree.isEmpty());
    }

    @Test
    @DisplayName("Put 10 elements and remove")
    void ListTreePut10ElementsAndRemoveTest() {
        var listTree = new ListTree<Integer, Double>();
        IntStream.range(0, 10).forEach(x -> listTree.put(x, x * 10.3405));
        assertEquals(10, listTree.size());
        IntStream.range(0, 10).filter(x -> x % 2 == 0).forEach(listTree::remove);
        assertEquals(5, listTree.size());
    }

    @Test
    @DisplayName("Put 100 k random elements and remove")
    void ListTreePut100KElementsAndRemoveTest() {
        var listTree = new ListTree<Integer, String>();
        var array = new Random(0)
            .ints(Integer.MIN_VALUE, Integer.MAX_VALUE)
            .limit(100000)
            .distinct()
            .toArray();
        for (var value : array) {
            listTree.put(value, String.valueOf(value));
            assertEquals(Optional.of(String.valueOf(value)), listTree.get(value));
        }
        assertEquals(array.length, listTree.size());
        for (var value : array) {
            assertEquals(Optional.of(String.valueOf(value)), listTree.remove(value));
            assertFalse(listTree.contains(value));
        }
        assertTrue(listTree.isEmpty());
    }
}
