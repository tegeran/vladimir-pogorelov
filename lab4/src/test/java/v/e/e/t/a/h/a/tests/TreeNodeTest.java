package v.e.e.t.a.h.a.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import v.e.e.t.a.h.a.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@DisplayName("Tree node")
public class TreeNodeTest {
    @Test
    @DisplayName("Single node with null data")
    void SingleTreeNodeOfNullTest() {
        final TreeNode<Object> node = TreeNode.withVal(null);
        assertNotNull(node);
        assertNull(node.getVal());
        assertEquals(node.getParent(), Optional.empty());
        final var children = node.getChildren();
        assertNotNull(children);
        assertEquals(0, children.size());
    }

    @Test
    @DisplayName("Single node with arbitrary data")
    void SingleTreeNodeTest() {
        final TreeNode<Integer> nodeWithInteger = TreeNode.withVal(6);
        assertEquals(Integer.valueOf(6), nodeWithInteger.getVal());
        nodeWithInteger.setVal(14);
        assertEquals(Integer.valueOf(14), nodeWithInteger.getVal());

        final TreeNode<String> nodeWithString = TreeNode.withVal("TeSt");
        assertEquals("TeSt", nodeWithString.getVal());
        nodeWithString.setVal("");
        assertEquals("", nodeWithString.getVal());

        final TreeNode<Double> nodeWithDouble = TreeNode.withVal(2.88034);
        assertEquals(Double.valueOf(2.88034), nodeWithDouble.getVal());
        nodeWithDouble.setVal(0D);
        assertEquals(Double.valueOf(0), nodeWithDouble.getVal());
    }

    @Test
    @DisplayName("Node with parent")
    void TreeNodeParentTest() {
        final TreeNode<Character> nodeA = TreeNode.withVal('A');
        final TreeNode<Character> nodeB = TreeNode.withVal('B');
        final TreeNode<Character> nodeC = TreeNode.withVal('C');

        nodeB.setParent(Optional.of(nodeA));
        nodeC.setParent(Optional.of(nodeA));
        assertEquals(Optional.of(nodeA), nodeB.getParent());
        assertEquals(Optional.of(nodeA), nodeC.getParent());

        nodeC.setParent(Optional.of(nodeB));
        assertEquals(Optional.of(nodeB), nodeC.getParent());

        nodeB.setParent(Optional.empty());
        assertEquals(nodeB.getParent(), Optional.empty());
    }

    @Test
    @DisplayName("Node linking children with parents")
    void TreeNodeLinkingTest() {
        final var nodeA = TreeNode.withVal('A');
        final var nodeB = TreeNode.withValAndParent('B', Optional.of(nodeA));
        final var nodeC = TreeNode.withValAndParent('C', Optional.of(nodeA));
        final var nodeD = TreeNode.withValAndParent('D', Optional.of(nodeA));

        final var children = nodeA.getChildren();
        assertThat(children, is(Arrays.asList(nodeB, nodeC, nodeD)));
        children.forEach(x -> assertEquals(x.getParent(), Optional.of(nodeA)));

        nodeA.addChild('E');
        final var nodeE = nodeA.getChild(3);
        assertEquals(Character.valueOf('E'), nodeE.getVal());
        assertEquals(Optional.of(nodeA), nodeE.getParent());

        nodeA.addChildren(Arrays.asList('F', 'G', 'H'));
        assertEquals(7, nodeA.getChildren().size());
        assertEquals(Optional.of(nodeA), nodeA.getChild(5).getParent());
    }
}
