package v.e.e.t.a.h.a.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import v.e.e.t.a.h.a.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@DisplayName("Traversal service test")
public class TraversalServiceTest {
    private TreeNode<Character> root;

    @BeforeEach
    void createTree() {
        root = TreeNode.withVal('F');
        var B = TreeNode.withValAndParent('B', Optional.of(root));
        var G = TreeNode.withValAndParent('G', Optional.of(root));
        B.addChild('A');
        var D = TreeNode.withValAndParent('D', Optional.of(B));
        D.addChildren(Arrays.asList('C', 'E'));
        var I = TreeNode.withValAndParent('I', Optional.of(G));
        // G.addChildNodes(Arrays.asList(null, I));
        I.addChild('H');
    }

    @Test
    @DisplayName("Depth first pre-order traversal single node")
    void DepthFirstTraversalSingleNodeTest() {
        TreeNode<Integer> node = TreeNode.withVal(1);
        assertThat(node.DepthFirstTraversal(), is(Collections.singletonList(1)));
    }


    @Test
    @DisplayName("Depth first pre-order traversal tree")
    void DepthFirstTraversalTreeTest() {
        assertThat(root.DepthFirstTraversal(), is(Arrays.asList('F', 'B', 'A', 'D', 'C', 'E', 'G', 'I', 'H')));
    }

    @Test
    @DisplayName("Breadth first traversal single node")
    void BreadthFirstTraversalSingleNodeTest() {
        TreeNode<Integer> node = TreeNode.withVal(6);
        assertThat(node.BreadthFirstTraversal(), is(Collections.singletonList(6)));
    }

    @Test
    @DisplayName("Breadth first traversal tree")
    void BreadthFirstTraversalTreeTest() {
        assertThat(root.BreadthFirstTraversal(), is(Arrays.asList('F', 'B', 'G', 'A', 'D', 'I', 'C', 'E', 'H')));
    }
}
