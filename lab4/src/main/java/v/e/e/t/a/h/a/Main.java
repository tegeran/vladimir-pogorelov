package v.e.e.t.a.h.a;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        var listTree = new ListTree<Integer, String>();
        var array = new Random(0)
            .ints(Integer.MIN_VALUE, Integer.MAX_VALUE)
            .limit(100000)
            .distinct()
            .toArray();
        for (var value : array) {
            listTree.put(value, String.valueOf(value));
        }
    }
}

