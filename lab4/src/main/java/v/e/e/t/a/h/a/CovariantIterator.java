// Copy-pasted from here: https://gist.github.com/cls/3cfaba9c6c185e976a48
package v.e.e.t.a.h.a;
import java.util.Iterator;

public class CovariantIterator<T extends U, U> implements Iterator<U>
{
    private Iterator<T> iter;

    public CovariantIterator(Iterator<T> it)
    {
        this.iter = it;
    }

    public CovariantIterator(Iterable<T> it)
    {
        this(it.iterator());
    }

    @Override
    public boolean hasNext()
    {
        return this.iter.hasNext();
    }

    @Override
    public U next()
    {
        return (U) this.iter.next();
    }

    @Override
    public void remove()
    {
        this.iter.remove();
    }
}
