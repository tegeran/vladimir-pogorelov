package v.e.e.t.a.h.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;


public class TraversalService {
    /**
     * This method is reentrant.
     */
    public static
    <T, TNode extends WithVal<T> & WithChildren<TNode>> // I wish there were some aliases ;(
    void traversePreOrder(
        final TNode root,
        final Consumer<? super T> visitor
    ) {
        new Consumer<TNode>() {
            public void accept(final TNode node) {
                visitor.accept(node.getVal());
                for (var child : node.getChildren()) {
                    this.accept(child);
                }
            }
        }.accept(root);
    }

    /**
     * This method is reentrant.
     */
    public static
    <T, TNode extends WithVal<T> & WithChildren<TNode>> // I wish there were some aliases ;(
    void traverseBreadthFirst(
        final TNode root,
        final Consumer<? super T> visitor
    ) {
        // store only references to lists of children instead of children themselves
        // kak tebe takoe Arkadiy Pauk?
        final var nodesToVisit = new LinkedList<Iterable<TNode>>();
        nodesToVisit.add(Arrays.asList(root));

        while (!nodesToVisit.isEmpty())
        for (final var node : nodesToVisit.remove()) {
            visitor.accept(node.getVal());
            nodesToVisit.add(node.getChildren());
        }
    }

    public static
    <T, TNode extends WithVal<T> & WithChildren<TNode>> // I wish there were some aliases ;(
    List<T> BreadthFirstTraversal(TNode root) {
        var nodes = new ArrayList<T>();
        TraversalService.traverseBreadthFirst(root, nodes::add);
        return nodes;
    }
}
