package v.e.e.t.a.h.a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class TreeNode<T> implements
Iterable<TreeNode<T>>,
WithParent<Optional<TreeNode<T>>>,
WithChildren<TreeNode<T>>,
WithVal<T>
{
// #region member variables
    T val;
    Optional<TreeNode<T>> parent = Optional.empty();
    final ArrayList<TreeNode<T>> children = new ArrayList<>();
//#endregion

// #region construction logic
    TreeNode(final T val) {
        this.val = val;
    }

    TreeNode(final T val, final Optional<TreeNode<T>> parent) {
        this(val);
        this.parent = parent;
        this.parent.ifPresent(p -> p.children.add(this));
    }

    public static <T> TreeNode<T> withVal(final T val) {
        return new TreeNode<T>(val);
    }

    public static <T> TreeNode<T> withValAndParent(final T val, final Optional<TreeNode<T>> parent) {
        return new TreeNode<T>(val, parent);
    }
// #endregion

// #region interface implementations
    @Override public T getVal() { return this.val; }
    @Override public void setVal(final T val) { this.val = val; }
    @Override public Iterator<TreeNode<T>> iterator() { return this.children.iterator(); }
    @Override public Optional<TreeNode<T>> getParent() { return this.parent; }
    @Override public void setParent(final Optional<TreeNode<T>> newParent) { this.parent = newParent; }
    @Override public List<TreeNode<T>> getChildren() { return this.children; }
// #endregion

// #region inherent methods
    public TreeNode<T> getChild(final int index) {
        return this.children.get(index);
    }

    public void addChild(T val) { TreeNode.withValAndParent(val, Optional.of(this)); }

    public void addChildren(final Collection<T> newChildValues) {
        newChildValues.forEach(val -> TreeNode.withValAndParent(val, Optional.of(this)));
    }
// #endregion
// #region required methods for lab
    public List<T> DepthFirstTraversal() {
        var nodes = new ArrayList<T>();
        TraversalService.traversePreOrder(this, nodes::add);
        return nodes;
    }
    public List<T> BreadthFirstTraversal() {
        var nodes = new ArrayList<T>();
        TraversalService.traverseBreadthFirst(this, nodes::add);
        return nodes;
    }
// #endregion
}
