package v.e.e.t.a.h.a;

public class RedBlackState<K extends Comparable<K>, V> {
    public K key;
    public V val;
    public RedBlackColor color;

    public RedBlackState(K key, V val, RedBlackColor color) {
        this.key = key;
        this.val = val;
        this.color = color;
    }

    public boolean is(RedBlackColor color) { return this.color == color; }

}
