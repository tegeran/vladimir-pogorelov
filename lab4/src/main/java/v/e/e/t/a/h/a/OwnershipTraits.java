package v.e.e.t.a.h.a;

import java.util.Collection;
import java.util.List;

interface WithVal<TVal> {
    TVal getVal();
    void setVal(TVal val);
}

interface WithParent<TParent> {
    TParent getParent();
    void setParent(TParent newParent);
}

interface WithChildren<TChild> extends Iterable<TChild> {
    List<TChild> getChildren();
}

// interface ITreeNode<T> extends
// WithValue<T>,
// WithChildren<ITreeNode<T>>
// {}

// interface ITreeNodeWithParent<T> extends
// ITreeNode<T>,
// WithParent<Optional<ITreeNode<T>>>
// {}


