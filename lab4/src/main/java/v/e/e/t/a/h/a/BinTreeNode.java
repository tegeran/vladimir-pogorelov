package v.e.e.t.a.h.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

class BinTreeNode<T> implements WithVal<T>, WithChildren<BinTreeNode<T>>, WithParent<Optional<BinTreeNode<T>>> {
    // #region member variables
    T val;
    Optional<BinTreeNode<T>> left = Optional.empty();
    Optional<BinTreeNode<T>> right = Optional.empty();
    Optional<BinTreeNode<T>> parent = Optional.empty();
    // #endregion

    BinTreeNode() {}

    static
    <K extends Comparable<K>, V>
    BinTreeNode<RedBlackState<K, V>> withParent(BinTreeNode<RedBlackState<K, V>> parent) {
        var newbie = new BinTreeNode<RedBlackState<K, V>>();
        newbie.val = new RedBlackState<>(null, null, RedBlackColor.Black);
        newbie.parent = Optional.of(parent);
        return newbie;
    }
    // #region construction

    BinTreeNode(final T val) {
        this.val = val;

    }

    public static 
    <K extends Comparable<K>, V> 
    BinTreeNode<RedBlackState<K, V>> withRBVal(final RedBlackState<K, V> val) {
        var newbie = new BinTreeNode<RedBlackState<K, V>>();
        newbie.val = val;
        var dummy = withParent(newbie);
        newbie.left = Optional.of(dummy);
        newbie.right = Optional.of(dummy);
        return newbie;
    }

    public static <T> BinTreeNode<T> withVal(final T val) {
        return new BinTreeNode<>(val);
    }

    // #endregion
    // #region interface implementations
    @Override
    public T getVal() {
        return this.val;
    }

    @Override
    public void setVal(final T val) {
        this.val = val;
    }

    @Override
    public Optional<BinTreeNode<T>> getParent() {
        return this.parent;
    }

    @Override
    public void setParent(final Optional<BinTreeNode<T>> newParent) {
        this.parent = newParent;
    }

    @Override
    public Iterator<BinTreeNode<T>> iterator() {
        return this.getChildren().iterator();
    }

    @Override
    public List<BinTreeNode<T>> getChildren() {
        var children = new ArrayList<BinTreeNode<T>>(2);
        this.left.ifPresent(children::add);
        this.right.ifPresent(children::add);
        return children;
    }

    // #endregion

//#region inherent methods
    boolean isLeft(BinTreeNode<T> other) {
        return this.isLeft(Optional.of(other));
    }
    boolean isLeft(Optional<BinTreeNode<T>> other) {
        return this.left.equals(other);
    }
    boolean isRight(BinTreeNode<T> other) {
        return !this.isLeft(other);
    }
    boolean isRight(Optional<BinTreeNode<T>> other) {
        return !this.isLeft(other);
    }


    void setLeft(Optional<BinTreeNode<T>> newLeft) { this.left = newLeft; }
    void setRight(Optional<BinTreeNode<T>> newRight) { this.right = newRight; }
    Optional<BinTreeNode<T>> getLeft() { return this.left; }
    Optional<BinTreeNode<T>> getRight() { return this.right; }
    Optional<BinTreeNode<T>> getSibling() { 
        return this.parent.flatMap(p -> p.left.equals(Optional.of(this)) ? p.right : p.left); 
    }
    Optional<BinTreeNode<T>> getGrandparent() {
        return this.parent.flatMap(BinTreeNode::getParent);
    }
    Optional<BinTreeNode<T>> getUncle() {
        return this.getParent().flatMap(BinTreeNode::getSibling);
    }

//#endregion
}
