package v.e.e.t.a.h.a;

import v.e.e.t.a.h.a.RedBlackColor;

import java.util.Optional;

import org.javatuples.Pair;


public class ListTree<K extends Comparable<K>, V> {
    private static final RedBlackColor Black = RedBlackColor.Black;
    private static final RedBlackColor Red = RedBlackColor.Red;
    private static <T> Optional<T> Some(T val) { return Optional.of(val); }
    private static <T> Optional<T> None() { return Optional.empty(); }


    private static final int TREEIFY_THRESHOLD = 8;
    private long count = 0;
    private Optional<BinTreeNode<RedBlackState<K, V>>> root = Optional.empty();

    public void put(K key, V value) {
        if (key == null || contains(key)) {
            return;
        }
        ++count;
        if (count == TREEIFY_THRESHOLD) {
            treeify();
            TraversalService.traverseBreadthFirst(root.orElseThrow(), v -> System.err.println(
                "key: " + (v.key == null ? "null" : v.key.toString()) +
                " value: " + (v.val == null ? "null" : v.val.toString()) +
                " color: " + (v.color == Black ? "black" : "red")
            ));
        }
        var rbState = new RedBlackState<>(key, value, Red);
        if (shouldBeTree()) {
            insertCase1(insertNodeInBST(rbState));
        } else {
            insertNodeInList(rbState);
        }
    }
    private boolean shouldBeTree() {
        return count >= TREEIFY_THRESHOLD;
    }

    public Optional<V> get(K key) {
        return findNodeInListOrTree(key).map(node -> node.getVal().val);
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public Optional<V> remove(K key) {
        return findNodeInListOrTree(key).map(node -> {
            var val = node.getVal().val;
            if (count == TREEIFY_THRESHOLD) {
                listify();
            }
            if (shouldBeTree()) {
                deleteNodeFromTree(node);
            } else {
                deleteNodeFromList(node);
            }
            --count;
            return val;
        });
    }

    public boolean contains(K key) {
        return findNodeInListOrTree(key).isPresent();
    }

    public long size() {
        return count;
    }

    private Pair<
        Optional<BinTreeNode<RedBlackState<K, V>>>,
        Optional<BinTreeNode<RedBlackState<K, V>>>
    > 
    findNodeAndAncestorInListOrTree(
        K expectedKey,
        Optional<BinTreeNode<RedBlackState<K, V>>> curNode,
        Optional<BinTreeNode<RedBlackState<K, V>>> prevNode
    ) {
        return curNode.filter(node -> node.getVal().key != null && expectedKey != null).map(node ->
            node.getVal().key.equals(expectedKey) ?
            new Pair<>(curNode, prevNode)         :
            (shouldBeTree() && expectedKey.compareTo(node.getVal().key) < 0)            ?
            this.findNodeAndAncestorInListOrTree(expectedKey, node.getLeft(),  curNode) :
            this.findNodeAndAncestorInListOrTree(expectedKey, node.getRight(), curNode)
        )
        .orElseGet(() -> new Pair<>(None(), prevNode));
    }

    private Optional<BinTreeNode<RedBlackState<K, V>>> findNodeInListOrTree(K expectedKey) {
        return findNodeAndAncestorInListOrTree(expectedKey, root, None()).getValue0();
    }

    private void insertNodeInList(RedBlackState<K, V> state) {
        var node = BinTreeNode.withRBVal(state);
        node.setRight(this.root);
        this.root.ifPresent(r -> r.setParent(Some(node)));
        this.root = Some(node);
    }

    private BinTreeNode<RedBlackState<K, V>> insertNodeInBST(RedBlackState<K, V> state) {
        var maybeParent = findNodeAndAncestorInListOrTree(state.key, root, None()).getValue1();
        var newNode = BinTreeNode.withRBVal(state);
        maybeParent.ifPresentOrElse(
            parent -> {
                newNode.setParent(Some(parent));
                if (state.key.compareTo(parent.getVal().key) < 0) {
                    parent.setLeft(Some(newNode));
                } else {
                    parent.setRight(Some(newNode));
                }
            },
            () -> this.root = Some(newNode)
        );
        return newNode;
    }

    private void deleteNodeFromList(BinTreeNode<RedBlackState<K, V>> node) {
        node.getParent().ifPresentOrElse(
            parent -> {
                parent.setRight(node.getRight());
                node.getRight().ifPresent(right -> right.setParent(Some(parent)));
            },
            () -> {
                this.root = node.getRight();
                this.root.ifPresent(root -> root.setParent(None()));
            }
        );
    }

    private void deleteNodeFromTree(BinTreeNode<RedBlackState<K, V>> node) {
        node.getRight()
            .filter(r -> r.getVal().key != null &&
            node.getLeft().isPresent() && 
            node.getLeft().orElseThrow().getVal().key != null
            )
            .ifPresentOrElse(
                right -> {
                    var minimumNode = getMinimumNodeHelper(right);
                    node.setVal(minimumNode.getVal());
                    deleteOneChild(minimumNode);
                },
                () -> deleteOneChild(node)
            );
    }

    private BinTreeNode<RedBlackState<K, V>> getMinimumNodeHelper(BinTreeNode<RedBlackState<K, V>> node) {
        return node.getLeft()
            .filter(n -> n.getVal().key != null)
            .map(this::getMinimumNodeHelper).orElse(node);
    }

    private void listify() {
        var prevRoot = this.root;
        this.root = None();
        prevRoot.ifPresent(this::listifyHelper);
    }

    private void listifyHelper(BinTreeNode<RedBlackState<K, V>> node) {
        var pair = node.getVal();
        this.insertNodeInList(new RedBlackState<>(pair.key, pair.val, Red));
        node.forEach(this::listifyHelper);
    }

    private void treeify() {
        var prevRoot = this.root;
        this.root = None();
        prevRoot.ifPresent(this::treeifyHelper);
    }

    private void treeifyHelper(BinTreeNode<RedBlackState<K, V>> node) {
        var pair = node.getVal();
        insertCase1(insertNodeInBST(new RedBlackState<>(pair.key, pair.val, Red)));
        node.getRight().ifPresent(this::treeifyHelper);
    }

    private void insertCase1(BinTreeNode<RedBlackState<K, V>> node) {
        node.getParent().ifPresentOrElse(
            p -> {
                if (p.getVal().is(Red)) { 
                    insertCase3(node);
                } else {
                    // Red black tree is valid
                }
            },
            () -> node.getVal().color = Black
        );
    }


    private void insertCase3(BinTreeNode<RedBlackState<K, V>> node) {
        node.getUncle().filter(u -> u.getVal().is(Red)).ifPresentOrElse(
            uncle -> {
                var parent = node.getParent().orElseThrow();
                var grandparent = node.getGrandparent().orElseThrow();
                parent.getVal().color = Black;
                uncle.getVal().color = Black;
                grandparent.getVal().color = Red;
                insertCase1(grandparent);
            },
            () -> insertCase4(node)
        );
    }

    private void insertCase4(BinTreeNode<RedBlackState<K, V>> node) {
        var parent = node.getParent().orElseThrow();
        if (parent.getRight().equals(Some(node)) && node.getGrandparent().orElseThrow().getLeft().equals(Some(parent))) {
            rotateLeft(parent);
            node = node.getLeft().orElseThrow();
        } else 
        if (parent.getLeft().equals(Some(node)) && node.getGrandparent().orElseThrow().getRight().equals(Some(parent))) {
            rotateRight(parent);
            node = node.getRight().orElseThrow();
        }
        insertCase5(node);
    }

    private void insertCase5(BinTreeNode<RedBlackState<K, V>> node) {
        var grandparent = node.getGrandparent().orElseThrow();
        var parent = node.getParent().orElseThrow();
        parent.getVal().color = Black;
        grandparent.getVal().color = Red;
        if (parent.getLeft().equals(Some(node)) && grandparent.getLeft().equals(Some(parent))) {
            rotateRight(grandparent);
        } else {
            rotateLeft(grandparent);
        }
    }

    private void replaceNode(BinTreeNode<RedBlackState<K, V>> node, BinTreeNode<RedBlackState<K, V>> child) {
        var parent = node.getParent().orElseThrow();
        child.setParent(Some(parent));
        if (parent.getLeft().equals(Some(node))) {
            parent.setLeft(Some(child));
        } else {
            parent.setRight(Some(child));
        }
    }

    private void deleteOneChild(BinTreeNode<RedBlackState<K, V>> node) {
        var child = node.getRight()
            .filter(n -> n.getVal().key != null)
            .or(node::getLeft)
            .filter(n -> n.getVal().key != null)
            .orElseThrow();

        replaceNode(node, child);
        if (node.getVal().is(Red)) { return; }
        if (child.getVal().is(Red)) {
            child.getVal().color = Black;
        } else {
            deleteCase1(child);
        }
    }

    private void deleteCase1(BinTreeNode<RedBlackState<K, V>> node) {
        node.getParent().ifPresent(_p -> deleteCase2(node));
    }

    private void deleteCase2(BinTreeNode<RedBlackState<K, V>> node) {
        var sibling = node.getSibling().orElseThrow(); // ?
        var parent = node.getParent().orElseThrow();
        if (sibling.getVal().is(Red)) {
            parent.getVal().color = Red;
            sibling.getVal().color = Black;
            if (parent.getLeft().equals(Some(node))) {
                rotateLeft(parent);
            } else {
                rotateRight(parent);
            }
        }
        deleteCase3(node);
    }

    private void deleteCase3(BinTreeNode<RedBlackState<K, V>> node) {
        var sibling = node.getSibling().orElseThrow();
        var parent = node.getParent().orElseThrow();
        if (    parent.getVal().is(Black)
            && sibling.getVal().is(Black)
            && sibling.getLeft() .map(l -> l.getVal().is(Black)).orElse(false)
            && sibling.getRight().map(r -> r.getVal().is(Black)).orElse(false)
        ) {
            sibling.getVal().color = Red;
            deleteCase1(parent);
        } else {
            deleteCase4(node);
        }
    }

    private void deleteCase4(BinTreeNode<RedBlackState<K, V>> node) {
        var sibling = node.getSibling().orElseThrow();
        var parent = node.getParent().orElseThrow();
        if (parent.getVal().is(Red)
            && sibling.getVal().is(Black)
            && sibling.getLeft() .map(l -> l.getVal().is(Black)).orElse(false)
            && sibling.getRight().map(r -> r.getVal().is(Black)).orElse(false)
        ) {
            sibling.getVal().color = Black;
        } else {
            deleteCase5(node);
        }
    }

    private void deleteCase5(BinTreeNode<RedBlackState<K, V>> node) {
        var sibling      = node.getSibling().orElseThrow();
        var siblingLeft  = sibling.getLeft().orElseThrow();
        var siblingRight = sibling.getRight().orElseThrow();
        var parent       = node.getParent().orElseThrow();
        if (sibling.getVal().is(Black)) {
            if (parent.isLeft(node) && siblingRight.getVal().is(Black) && siblingLeft.getVal().is(Red)) {
                sibling.getVal().color = Red;
                siblingLeft.getVal().color = Black;
                rotateRight(sibling);
            } else if (parent.isRight(node) && siblingLeft.getVal().is(Black) && siblingRight.getVal().is(Red)) {
                sibling.getVal().color = Red;
                siblingRight.getVal().color = Black;
                rotateLeft(sibling);
            }
        }
        deleteCase6(node);
    }

    private void deleteCase6(BinTreeNode<RedBlackState<K, V>> node) {
        var sibling = node.getSibling().orElseThrow();
        var parent = node.getParent().orElseThrow();
        sibling.getVal().color = parent.getVal().color;
        parent.getVal().color = Black;
        if (parent.isLeft(node)) {
            sibling.getRight().orElseThrow().getVal().color = Black;
            rotateLeft(parent);
        } else {
            sibling.getLeft().orElseThrow().getVal().color = Black;
            rotateRight(parent);
        }
    }

//#region rotations
    private void rotateLeft(BinTreeNode<RedBlackState<K, V>> node) {
        var pivot = node.getRight().orElseThrow();
        pivot.setParent(node.getParent());
        if (root.equals(Some(node))) {
            root = Some(pivot);
        }
        changeParentChild(pivot, node);
        node.setRight(pivot.getLeft());
        pivot.getLeft().ifPresent(l -> l.setParent(Some(node)));
        node.setParent(Some(pivot));
        pivot.setLeft(Some(node));
    }

    private void rotateRight(BinTreeNode<RedBlackState<K, V>> node) {
        var pivot = node.getLeft().orElseThrow();
        pivot.setParent(node.getParent());
        if (root.equals(Some(node))) {
            root = Some(pivot);
        }
        changeParentChild(pivot, node);
        node.setLeft(pivot.getRight());
        pivot.getRight().ifPresent(r -> r.setParent(Some(node)));
        node.setParent(Some(pivot));
        pivot.setRight(Some(node));
    }

    private static
    <K extends Comparable<K>, V>
    void changeParentChild(
        BinTreeNode<RedBlackState<K, V>> pivot,
        BinTreeNode<RedBlackState<K, V>> node
    ) {
        node.getParent().ifPresent(p -> {
            if (p.getLeft().equals(Some(node))) {
                p.setLeft(Some(pivot));
            } else {
                p.setRight(Some(pivot));
            }
        });
    }
//#endregion

}
