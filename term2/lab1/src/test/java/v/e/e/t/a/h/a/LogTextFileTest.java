package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

@DisplayName("Log text file")
public class LogTextFileTest {
    @Test
    @DisplayName("new file")
    void createFileTest() {
        var dir = new Dir("some");

        var file = new LogTextFile("cont", Optional.of(dir), "line");
        assertNotNull(file);

        assertEquals("cont", file.getName());
    }

    @Test
    @DisplayName("Read and append")
    void readAndAppendTest() {
        var dir = new Dir("dir");
        var file = new LogTextFile("file", Optional.of(dir), "initial log");

        assertEquals("initial log", file.read());
        file.append("\nappended");
        assertEquals("initial log\nappended", file.read());
        file.append("");
        assertEquals("initial log\nappended", file.read());
    }
}
