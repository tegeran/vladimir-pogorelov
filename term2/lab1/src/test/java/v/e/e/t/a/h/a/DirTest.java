package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Dir")
public class DirTest {
    @Test
    @DisplayName("Create directory")
    void createDirectoryTest() {
        var dir = new Dir("root");
        assertNotNull(dir);

        assertEquals("root", dir.getName());

        var subDir = new Dir("tmp", Optional.of(dir));
        assertEquals("tmp", subDir.getName());
    }

    @Test
    @DisplayName("list directory")
    void listDirectoryTest() {
        var root = new Dir("root");
        var listElements = new ArrayList<Dirent>();
        listElements.add(new Dir("sub", Optional.of(root)));
        listElements.add(new BinaryFile("file.bin", Optional.of(root), "data".getBytes()));
        listElements.add(new LogTextFile("log", Optional.of(root), "line"));
        assertArrayEquals(listElements.toArray(), root.list().toArray());
        assertArrayEquals(Collections.emptyList().toArray(), new Dir("dir").list().toArray());
    }

    @Test
    @DisplayName("remove directory")
    void removeDirectoryTest() {
        var root = new Dir("root");
        var file = new BinaryFile("file", Optional.of(root), "data".getBytes());
        var subDir = new Dir("var", Optional.of(root));

        assertThrows(AssertionError.class, root::remove);
        assertDoesNotThrow(() -> subDir.remove());
        assertDoesNotThrow(() -> file.remove());
        assertDoesNotThrow(() -> root.remove());
    }

    @Test
    @DisplayName("move dir")
    void moveTest() {
        var root = new Dir("root");
        var subDir = new Dir("folder", Optional.of(root));
        var anotherSubDir = new Dir("dir", Optional.of(root));
        var log = new LogTextFile("l.txt", Optional.of(subDir), "\n");

        assertThrows(AssertionError.class, () -> Dirent.move(subDir, root));
        assertTrue(!subDir.list().contains(anotherSubDir));
        assertTrue(subDir.list().contains(log));
    }

    @Test
    @DisplayName("directory overflow")
    void overflowTest() {
        var root = new Dir("root");

        assertThrows(AssertionError.class, () -> {
            for (int i = 0; i < 1000; i++) {
                new BufferFile("buf file #" + i, Optional.of(root));
            }
        });
    }
}
