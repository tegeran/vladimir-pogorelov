package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

@DisplayName("File system")
public class DirentTest {
    @Test
    @DisplayName("Move file")
    void moveFilesTest() {
        var root = new Dir("root");
        var swap = new SwapFile("swap", Optional.of(root));
        var homeDir = new Dir("home", Optional.of(root));
        var userDir = new Dir("user", Optional.of(homeDir));


        Dirent.move(userDir, swap);
        assertFalse(userDir.list().isEmpty());
        assertEquals(swap, userDir.list().iterator().next());
    }

    @Test
    @DisplayName("Remove")
    void removeTest() {
        var root = new Dir("root");
        var homeDir = new Dir("home", Optional.of(root));
        var swap = new SwapFile("pipe", Optional.of(homeDir));
        var tempDir = new Dir("user", Optional.of(homeDir));

        swap.remove();
        assertFalse(homeDir.list().contains(swap));
        assertTrue(homeDir.list().contains(tempDir));

        tempDir.remove();
        assertFalse(homeDir.list().contains(tempDir));

        homeDir.remove();
        assertTrue(root.list().isEmpty());
    }
}
