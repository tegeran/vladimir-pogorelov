package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

@DisplayName("SwapFile")
public class SwaptFileTest {
    @Test
    @DisplayName("new SwapFile")
    void createFileTest() {
        var dir = new Dir("some");

        var file = new SwapFile("swap test file", Optional.of(dir));
        assertNotNull(file);
        assertEquals("swap test file", file.getName());
    }


    @Test
    @DisplayName("Read write")
    void readWriteTest() {
        var dir = new Dir("dir");
        var file = new SwapFile("file", Optional.of(dir));

        assertEquals("", file.read());
        file.write("swap contents ");
        assertEquals("swap contents ", file.read());
    }
}
