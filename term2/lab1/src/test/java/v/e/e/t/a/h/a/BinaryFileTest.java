package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

@DisplayName("Binary file")
public class BinaryFileTest {
    @Test
    @DisplayName("New file")
    void newFileTest() {
        var dir = new Dir("some");

        var file = new BinaryFile("f", Optional.of(dir), "test data".getBytes());
        assertNotNull(file);
        assertEquals("f", file.getName());
    }

    @Test
    @DisplayName("Read")
    void readTest() {
        var dir = new Dir("d", Optional.empty());
        var file = new BinaryFile("f", Optional.of(dir), "test data".getBytes());
        assertArrayEquals("test data".getBytes(), file.read());

        var emptyFile = new BinaryFile("k", Optional.of(dir), "".getBytes());
        assertEquals(0, emptyFile.read().length);
    }
}
