package v.e.e.t.a.h.a;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import v.e.e.t.a.h.a.BufferFile;
import v.e.e.t.a.h.a.Dir;
import v.e.e.t.a.h.a.Dirent;

import java.util.EmptyStackException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class BufferFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var dir = new Dir("some");

        var file = new BufferFile("buffer_test", Optional.of(dir));
        assertNotNull(file);
        assertEquals("buffer_test", file.getName());
    }

    @Test
    @DisplayName("Poll and add")
    void pollAndAddTest() {
        var dir = new Dir("dir");
        var file = new BufferFile("file", Optional.of(dir));

        assertEquals(file.poll(), Optional.empty());
        file.add('Q');
        file.add('A');
        assert('Q' == file.poll().get().charValue());
        assert('A' == file.poll().get().charValue());
        assertEquals(file.poll(), Optional.empty());
    }

    @Test
    @DisplayName("Buffer overflow test")
    void overflowTest() {
        var dir = new Dir("dir");
        var file = new BufferFile("file", Optional.of(dir));

        assertThrows(AssertionError.class, () -> {
            for (int i = 0; i < 1000; ++i) {
                file.add('A');
            }
        });
    }
}
