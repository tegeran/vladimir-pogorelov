package v.e.e.t.a.h.a;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Dir extends Dirent {
    private static final int DIR_MAX_ELEMS = 25;

    ReadWriteLock childrenLock = new ReentrantReadWriteLock();

    private final Map<String, Dirent> children = new HashMap<>(DIR_MAX_ELEMS);

    Dir(final String name) {
        super(name);
    }

    Dir(final String name, final Optional<Dir> parent) {
        super(name, parent);
    }

    protected boolean contains(final String name) {
        try {
            this.childrenLock.readLock().lock();
            return children.containsKey(name);
        } finally {
            this.childrenLock.readLock().unlock();
        }
    }

    protected int size() {
        try {
            this.childrenLock.readLock().lock();
            return children.size();
        } finally {
            this.childrenLock.readLock().unlock();
        }
    }

    protected void add(final Dirent child) {
        assert size() <= DIR_MAX_ELEMS;

        try {
            this.childrenLock.writeLock().lock();
            children.put(child.getName(), child);
        } finally {
            this.childrenLock.writeLock().unlock();
        }
    }

    protected Optional<Dirent> remove(final String name) {
        try {
            this.childrenLock.writeLock().lock();
            return Optional.ofNullable(children.remove(name));
        } finally {
            this.childrenLock.writeLock().unlock();
        }
    }

    protected void rename(final String oldName, final String newName) {
        try {
            this.childrenLock.writeLock().lock();

            final var child = remove(oldName).get();
            child.rename(newName);

            children.put(newName, child);

        } finally {
            this.childrenLock.writeLock().unlock();
        }
    }

    public Collection<Dirent> list() {
        try {
            this.childrenLock.readLock().lock();

            return children.values();
        } finally {
            this.childrenLock.readLock().unlock();
        }

    }

    @Override
    public void remove() {
        assert size() == 0;
        super.remove();
    }
}
