package v.e.e.t.a.h.a;

import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LogTextFile extends Dirent {
    private StringBuilder data;
    private ReadWriteLock rwlock = new ReentrantReadWriteLock();

    public LogTextFile(String name, Optional<Dir> parent, String content) {
        super(name, parent);
        this.data = new StringBuilder(content);
    }

    public String read() {
        try {
            this.rwlock.readLock().lock();
            return data.toString();
        } finally {
            this.rwlock.readLock().unlock();
        }
    }

    public synchronized void append(String line) {
        try {
            this.rwlock.writeLock().lock();
            data.append(line);
        } finally {
            this.rwlock.writeLock().unlock();
        }
    }
}
