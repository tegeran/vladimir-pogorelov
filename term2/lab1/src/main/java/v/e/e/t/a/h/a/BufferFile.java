package v.e.e.t.a.h.a;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

public class BufferFile extends Dirent {
    private final static int MAX_BUF_FILE_SIZE = 10;

    private Queue<Character> que = new LinkedList<>();

    BufferFile(String name) { super(name); }
    BufferFile(String name, Optional<Dir> parent) { super(name, parent); }

    public synchronized void add(char ch) {
        assert que.size() <= MAX_BUF_FILE_SIZE;
        que.add(ch);
    }

    public synchronized Optional<Character> poll() {
        return Optional.ofNullable(que.poll());
    }
}
