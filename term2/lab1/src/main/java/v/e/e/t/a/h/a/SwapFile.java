package v.e.e.t.a.h.a;

import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SwapFile extends Dirent {
    private String data;
    private ReadWriteLock rwlock = new ReentrantReadWriteLock();

    public SwapFile(String name, Optional<Dir> parent) {
        super(name, parent);
        data = "";
    }

    public void write(String data) {
        try {
            this.rwlock.writeLock().lock();
            this.data = data;
        }  finally {
            this.rwlock.writeLock().unlock();
        }
    }

    public String read() {
        try {
            this.rwlock.readLock().lock();
            return data;
        } finally {
            this.rwlock.readLock().unlock();
        }
    }
}
