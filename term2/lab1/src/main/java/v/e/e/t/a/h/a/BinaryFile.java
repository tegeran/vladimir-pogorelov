package v.e.e.t.a.h.a;

import java.util.Optional;

public class BinaryFile extends Dirent {
    private byte[] data;

    public BinaryFile(String name, Optional<Dir> parent, byte[] data) {
        super(name, parent);
        this.data = data;
    }

    public byte[] read() {
        return data;
    }
}
