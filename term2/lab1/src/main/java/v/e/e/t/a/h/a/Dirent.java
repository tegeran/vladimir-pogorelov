package v.e.e.t.a.h.a;

import java.util.Optional;

public abstract class Dirent {
    private Optional<Dir> parent;
    private String name;

    protected Dirent(String name) {
        this(name, Optional.empty());
    }

    protected Dirent(String name, Optional<Dir> parent) {
        parent.ifPresent(p -> { assert !p.contains(name); });
        assert !name.isEmpty() && !name.contains("/");

        this.name = name;
        this.parent = parent;

        parent.ifPresent(p -> p.add(this));
    }

    protected Optional<Dir> getParent() {
        return parent;
    }

    protected void rename(String name) {
        assert !name.isEmpty();

        var oldName = this.name;
        this.name = name;

        parent.ifPresent(p -> p.rename(oldName, this.name));
    }


    public void remove() {
        parent.ifPresent(p -> p.remove(name));
        parent = Optional.empty();
    }

    public static void move(Dir dest, Dirent src) {
        if (src instanceof Dir) {
            assert ((Dir)src).size() == 0;
        }

        src.parent.ifPresent(p -> p.remove(src.name));
        dest.add(src);
        src.parent = Optional.of(dest);
    }

    public String getName() {
        return name;
    }
}
