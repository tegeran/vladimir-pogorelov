package v.e.e.t.a.h.a;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class Main {
    public static void main(final String[] args) {
        System.out.println("TASK 2");

        final var root = new Dir("root"); {
            final var dir1 = new Dir("dir1", Optional.of(root)); {
                final var binary = new BinaryFile("binary", Optional.of(dir1), "BINARY_DATA".getBytes());
                final var buffer = new BufferFile("buffer", Optional.of(dir1));
                for (final var c : "inital buffer data".toCharArray()) buffer.add(c);
            }
            final var dir2 = new Dir("dir2", Optional.of(root)); {
                final var inner = new Dir("leaf"); {
                    final var log = new LogTextFile("log", Optional.of(inner), "logs here");
                    final var swap = new SwapFile("swap", Optional.of(inner));
                }
            }
        }

        for (final var dir: root.list()) {
            System.out.println(dir.getName());
        }

        System.out.println("TASK 3");

        var tasks = new Task[] {
            new Task("Thread 1") {
                public void afterTask() throws Exception {
                    finalize34.await();
                }
                public void afterFinalize() throws Exception {
                    finalize.countDown();
                }
            },
            new Task("Thread 2") {
                public void afterTask() throws Exception {
                    finalize34.await();
                }
                public void afterFinalize() throws Exception {
                    finalize.countDown();
                }
            },
            new Task("Thread 3") {
                public void afterTask() { }
                public void afterFinalize() throws Exception {
                    finalize34.countDown();
                    finalize.countDown();
                }
            },
            new Task("Thread 4") {
                public void afterTask() { }
                public void afterFinalize() throws Exception {
                    finalize34.countDown();
                    finalize.countDown();
                }
            },
            new Task("Thread 5") {
                public void afterTask() throws Exception {
                    finalize.await();
                }
                public void afterFinalize() { }
            }
        };

        for (var task: tasks) task.start();

        try {
            for (var task : tasks) task.join();
        } catch (InterruptedException err) {
            err.printStackTrace();
        }
    }
}

abstract class Task extends Thread {
    private final String name;
    public static CyclicBarrier initBarier = new CyclicBarrier(5);
    public static CountDownLatch finalize34 = new CountDownLatch(2);
    public static CountDownLatch finalize = new CountDownLatch(4);

    public Task(final String name) {
        this.name = name;
    }

    public void afterInit() throws Exception {
        initBarier.await();
    }

    public abstract void afterTask() throws Exception;

    public abstract void afterFinalize() throws Exception;


    @Override
    public void run() {
        try {
            task_init();
            afterInit();
            task();
            afterTask();
            task_finalize();
            afterFinalize();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private void doWork(final String fnName) {
        System.out.println(this.name + " is now running" + fnName);
        try {
            Thread.sleep(3000);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.name +" ended running" + fnName);
    }

    public void task_init() {
        doWork("task_init()");
    }

    public void task() {
        doWork("task()");
    }

    public void task_finalize() {
        doWork("task_finalize()");
    }
}
